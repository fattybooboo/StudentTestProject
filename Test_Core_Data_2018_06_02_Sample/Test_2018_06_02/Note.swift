//
//  Note.swift
//  Test_2018_06_02
//
//  Created by 劉家維 on 2018/6/2.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import Foundation
import UIKit
import CoreData
class Note: NSManagedObject {
    
    static func ==(lhs: Note, rhs: Note) -> Bool {
        return lhs === rhs
    }
    
    
    @NSManaged var text : String?
    @NSManaged var imageName : String? //self.noteID.jpg
    @NSManaged var noteID : String
    override func awakeFromInsert() {
        self.noteID = UUID().uuidString
    }
    
    
    
    //MARK: - 從檔案讀出轉成UIImage
    func image() -> UIImage? {
        if let name = self.imageName{
        let homeUrl = URL(fileURLWithPath: NSHomeDirectory())
        let docUrl = homeUrl.appendingPathComponent("Documents")
        let fileUrl = docUrl.appendingPathComponent(name)
            return UIImage(contentsOfFile: fileUrl.path)
        }
        return nil
    }
    
    //MARK: - 縮圖 講義p.262
    func thumbnail() -> UIImage? {
        
        if let image =  self.image() {
            
            let thumbnailSize = CGSize(width:50, height: 50); //設定縮圖大小
            let scale = UIScreen.main.scale //找出目前螢幕的scale，視網膜技術為2.0
            
            //產生畫布，第一個參數指定大小,第二個參數true:不透明（黑色底）,false表示透明背景,scale為螢幕scale
            UIGraphicsBeginImageContextWithOptions(thumbnailSize,false,scale)
            
            //計算長寬要縮圖比例，取最大值MAX會變成UIViewContentModeScaleAspectFill
            //最小值MIN會變成UIViewContentModeScaleAspectFit
            let widthRatio = thumbnailSize.width / image.size.width;
            let heightRadio = thumbnailSize.height / image.size.height;
            
            let ratio = max(widthRatio,heightRadio);
            
            let imageSize = CGSize(width:image.size.width*ratio,height: image.size.height*ratio);
            
            image.draw(in:CGRect(x: -(imageSize.width-thumbnailSize.width)/2.0,y: -(imageSize.height-thumbnailSize.height)/2.0,
                                 width: imageSize.width,height: imageSize.height))
            //取得畫布上的縮圖
            let smallImage = UIGraphicsGetImageFromCurrentImageContext();
            //關掉畫布
            UIGraphicsEndImageContext();
            return smallImage
        }else{
            return nil;
        }
    }
}
