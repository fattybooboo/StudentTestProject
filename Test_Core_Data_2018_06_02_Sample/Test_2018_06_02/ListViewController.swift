//
//  ViewController.swift
//  Test_2018_06_02
//
//  Created by 劉家維 on 2018/6/2.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit
import CoreData

class ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NoteViewControllerDelegate {
    
    //Prepare tableView.
    @IBOutlet weak var tableView: UITableView!
    var data : [Note] = []
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //TODO: - 呼叫queryFromCoreData().
        self.queryFromCoreData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        
        //Prepare navigationItem
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        
        self.tableView.delegate = self
        
    }
    //MARK: - setEditing.
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: true)
    }
    
    
    //MARK: - commit editingStyle.(刪除資料)
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let note = self.data.remove(at: indexPath.row)
            let context = CoreDataHelper.shared.managedObjectContext()
            context.delete(note)
            //TODO: - 設定CoreData.
            CoreDataHelper.shared.saveContext()
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    //REMARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //設定cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let note = data[indexPath.row]
        cell.textLabel?.text = note.text
        //TODO: - 在cell顯示縮圖.
        //設定縮圖.
        cell.imageView?.image = note.thumbnail()
        cell.showsReorderControl = true
        return cell
        
    }
    
    //MARK: - moveRowAt.
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let note = self.data.remove(at: sourceIndexPath.row) //移除sourceIndexPath的note
        self.data.insert(note, at: destinationIndexPath.row) //插入在destination的位置
        //TODO: 設定 CoreData.
        CoreDataHelper.shared.saveContext()
        
        //測試
        self.data.forEach{
            (note) in print(note.text as Any)
        }
    }
    //MARK: addNote.(增加資料)
    @IBAction func addNote(_ sender: Any) {
        let context = CoreDataHelper.shared.managedObjectContext()
        let note = NSEntityDescription.insertNewObject(forEntityName: "Note", into: context) as! Note
        note.text = "New"
        self.data.append(note)
        //TODO: - 設定CoreData.
        CoreDataHelper.shared.saveContext()
        let indexPath = IndexPath(row: self.data.count-1, section: 0)
        self.tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    //MARK: - Prepare Segue.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "noteSegue" {
            let noteVC = segue.destination as! NoteViewController
            if let  indexPath = self.tableView.indexPathForSelectedRow {
                let note = self.data[indexPath.row]
                noteVC.note = note
                noteVC.delegate = self
            }
        }
    }
    
    //MARK: - didFinishUpdate.(修改資料)
    func didFinishUpdate(note : Note) {
        //Test.
        //print("取得新的值\(note.text!)")
        
        //知道 note在原本self.data的順序(先設定Equatable)
        let index = self.data.index(of: note)
        
        //IndexPath.
        //reloadRow tableView.
        if let index1 = index {
           let indexPath = IndexPath(row: index1, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        //TODO: - 設定CoreData.
        CoreDataHelper.shared.saveContext()
    
    }
    
    
    //REMARK: - CoreData
    func queryFromCoreData() {
        let context = CoreDataHelper.shared.managedObjectContext();
        let request = NSFetchRequest<Note>.init(entityName: "Note")
        context.performAndWait {
            do {
                let result : [Note] = try context.fetch(request)
                self.data = result
            } catch {
                print("error while fetching coredata \(error)")
                self.data = []
            }
        }
    }
    
    
    //REMARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

