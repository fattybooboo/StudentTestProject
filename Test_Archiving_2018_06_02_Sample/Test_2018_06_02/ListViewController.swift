//
//  ViewController.swift
//  Test_2018_06_02
//
//  Created by 劉家維 on 2018/6/2.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NoteViewControllerDelegate {
    
    //Prepare tableView.
    @IBOutlet weak var tableView: UITableView!
    var data : [Note] = []
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //TODO: - 設定 Archiving.
        self.loadFromFile()
        
//        for index in 1...10 {
//            let fakeNote = Note()
//            fakeNote.text = "Note \(index)"
//            data.append(fakeNote)
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        
        //Prepare navigationItem
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        
        self.tableView.delegate = self
        
    }
    
    
    //MARK: - setEditing.
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: true)
    }
    
    
    //MARK: - commit editingStyle.(刪除資料)
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.data.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        //TODO: - 設定Archiving.
        self.saveToFile()
        
    }
    
    //REMARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //設定cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let note = data[indexPath.row]
        cell.textLabel?.text = note.text
        //TODO: - 設定縮圖.(Note.swift)
        cell.imageView?.image = note.thumbnail()
        cell.showsReorderControl = true
        return cell
        
    }
    //MARK: - moveRowAt.(修改資料)
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let note = self.data.remove(at: sourceIndexPath.row)
        self.data.insert(note, at: destinationIndexPath.row)
        
        //TODO: 設定 Archiving.
        self.saveToFile()
    }
    
    //MARK: - addNote.
    @IBAction func addNote(_ sender: Any) {
        let note = Note()
        note.text = "New"
        self.data.append(note)
        let indexPath = IndexPath(row: self.data.count-1, section: 0)
        self.tableView.insertRows(at: [indexPath], with: .automatic)
        
        //TODO: - 設定Archiving.
        self.saveToFile()
    }
    
    //MARK: - Prepare Segue.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "noteSegue" {
            let noteVC = segue.destination as! NoteViewController
            if let  indexPath = self.tableView.indexPathForSelectedRow {
                let note = self.data[indexPath.row]
                noteVC.note = note
                noteVC.delegate = self
            }
        }
    }
    
    //MARK: - didFinishUpdate.(修改資料)
    //接受通知.
    func didFinishUpdate(note : Note) {
        //Test.
        //print("取得新的值\(note.text!)")
        
        //知道 note在原本self.data的順序(先設定Equatable)
        let index = self.data.index(of: note)
        
        //IndexPath.
        //reloadRow tableView.
        if let index1 = index {
           let indexPath = IndexPath(row: index1, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        //TODO: - 設定Archiving.
        self.saveToFile()
    
    }
    
    
    //REMARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    //MARK: - 設定Archiving.
    func saveToFile() {
        //儲存位置
        let homeUrl = URL(fileURLWithPath: NSHomeDirectory()) //把String轉成URL
        let docUrl = homeUrl.appendingPathComponent("Documents")
        let fileUrl = docUrl.appendingPathComponent("notes.txt")
        
        NSKeyedArchiver.archiveRootObject(self.data, toFile: fileUrl.path)
    }
    
    func loadFromFile() {
        let homeUrl = URL(fileURLWithPath: NSHomeDirectory()) //把String轉成URL
        let docUrl = homeUrl.appendingPathComponent("Documents")
        let fileUrl = docUrl.appendingPathComponent("notes.txt")
        do {
            
        let data = try Data(contentsOf: fileUrl) //把URL轉成Data.
            if let array = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Note] {
                self.data = array
            }else{
                self.data = []
            }
            
        }catch{
            print("error\(error)")
        }
    
    
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

