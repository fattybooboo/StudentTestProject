//
//  NoteViewController.swift
//  Test_2018_06_02
//
//  Created by 劉家維 on 2018/6/2.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit


protocol NoteViewControllerDelegate : class {
    func didFinishUpdate(note: Note)
}


class NoteViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var textView: UITextView!
    var note : Note!
    weak var delegate : NoteViewControllerDelegate?
    @IBOutlet weak var ImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.text = self.note.text
        self.ImageView.image = self.note.image()
        
        //邊框 講義p.267
        self.ImageView.layer.borderWidth = 10
        self.ImageView.layer.borderColor = UIColor.blue.cgColor
        //圓角 講義p.268
        //self.ImageView.layer.cornerRadius = 10 //圓角半徑
        //self.ImageView.layer.masksToBounds = true //設定圓角一定要設
        
        //陰影 講義p.269 不能與圓角併存
        self.ImageView.layer.shadowColor = UIColor.darkGray.cgColor
        self.ImageView.layer.shadowOpacity = 0.8
        self.ImageView.layer.shadowOffset = CGSize(width: 10, height: 10)
        
        
        
    }
    //MARK: - done.
    //Prepare Segue.
    @IBAction func done(_ sender: UIBarButtonItem) {
        //把目前畫面的文字更新回note物件.
        self.note.text = self.textView.text
        //self.note.image = self.ImageView.image
        
        //儲存照片.
        //1.照片存在Documents下,檔名{uuid}.jpg
        //2.把檔名放在note.imageName上
        if let image = self.ImageView.image {
            
            //儲存位置.
            let homeUrl = URL(fileURLWithPath: NSHomeDirectory()) //把String轉成URL
            let docUrl = homeUrl.appendingPathComponent("Documents")
            let fileName = String(format: "%@.jpg", self.note.noteID)
            self.note.imageName = fileName
            let fileUrl = docUrl.appendingPathComponent(fileName)
            //Check.
            print("home \(NSHomeDirectory())") //.path(URL)會轉成String
            
            //設定縮圖 ->Note.swift.
            
            //轉為jpg.
            if let data = UIImageJPEGRepresentation(image, 1){
                do{
                    try data.write(to: fileUrl,options: .atomic)
                }catch{
                    print("error\(error)")
                }
            }
        }
        
        self.delegate?.didFinishUpdate(note: self.note)
        //回到前一頁 講義p.237
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    //MARK: - camera.
    @IBAction func camera(_ sender: UIBarButtonItem) {
       let controller = UIImagePickerController()
        controller.sourceType = .savedPhotosAlbum
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    
    //REMARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //1.先設定隱私權project editor/ info/ Privacy - Photo Library Usage Description
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.ImageView.image = image
            
        }
        //把選照片的Controller dissmiss
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
