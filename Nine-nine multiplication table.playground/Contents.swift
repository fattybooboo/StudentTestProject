//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

var a:Int = 1

var b:Int = 1


for i in a...9 {
    for j in b...9 {
        let s = i * j < 10 ? " " : ""
        print ("\(i)*\(j)=\(s)","\(i*j)",terminator: " ")
    }
    print()
}
